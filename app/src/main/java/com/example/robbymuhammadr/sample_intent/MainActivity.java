package com.example.robbymuhammadr.sample_intent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    private Button btnModul1, btnModul2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnModul1 = (Button) findViewById(R.id.activity_tugas_hitung_luas);
        btnModul2 = (Button)findViewById(R.id.activity_tugas3);

        btnModul1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TugasHitungLuas.class);
                startActivity(intent);
            }
        });


        btnModul2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Tugas3Activity.class);
                startActivity(intent);
            }
        });
    }
}
